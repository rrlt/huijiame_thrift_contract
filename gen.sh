#!/bin/bash
#
# execute ./gen.sh Order gen order client code
#
/usr/local/bin/thrift -gen php -out php/ thrift/$1.thrift
/usr/local/bin/thrift -gen java -out java/ thrift/$1.thrift
#/usr/local/bin/thrift -gen js -out js/Service/$1 thrift/$1.thrift
gradle clean install
#upload