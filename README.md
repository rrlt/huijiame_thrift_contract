# 执行gen.sh生成代码

参数为thrift文件名,例如: ./gen.sh Order

---

# 根据idl生成java代码
  
项目根目录下 thrift -gen java  -out java/ thrift/Order.thrift  

默认生成在/java目录下

---

# 根据idl生成php代码
  
项目根目录下 thrift -gen php  -out php/ thrift/Order.thrift   

默认生成在/php目录下  

---
# 如何在自己的项目中添加此submodule

git submodule add https://yangjunming@bitbucket.org/rrlt/huijiame_thrift_contract.git huijiame_thrift_contract
