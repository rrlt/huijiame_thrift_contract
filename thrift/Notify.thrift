namespace java com.huijiame.soa.notify.thrift
namespace php Service.Notify

/*
TODO push body by baidu 

fromUserId sys default 100007

*/
/** 系统公告 & 通知群发 */
struct SysNotice {
    1: required i32 clientId,			 	 	 // 参考 DB huijiame.h_application.id
    2: required NoticeRange noticeRange,   	 // 广播范围
    3: required NoticeType noticeType,	 	 // 广播类型
	4: optional Message message,				 // 站内信
	5: optional PushMessage pushMessage,		 // 推送消息
    6: optional list<i32> users, 				 // 接收者
    7: optional i32 sendTime,				 	 // 发送时间,单位秒（必须在当前时间60s以外，1年(31536000s)以内，空值或不传为立即执行）
    8: optional byte referenceType,			 // 后台专用
    9: optional i32 referenceId,			     // 后台专用
}

/** 站内信 */
struct Message {
    1: required i16 cateType,
    2: required i16 type,
    3: required string title,
    4: required string content,
    5: required i32 senderId,			 		// 站内信必传 发送者
    6: required i32 userId,				   //  接收 (群发，设置-1)
    7: optional string image,
    8: optional string remark,  
    9: optional i32 readStatus,
    10: optional i64 createTime,        // 返回数据持有
    11: optional string objectId,		 //  消息id
    12: optional Source source,
    13: optional string url,
}

/** 发送站内信请求 */
struct SendMessageRequest {
	1: required i32 clientId,
	2: required list<Message> messages,
	3: optional i32 sendTime,	 // 发送时间,单位秒（必须在当前时间60s以外，1年(31536000s)以内，空值或不传为立即执行）
	4: optional i64 requestId, // 如需追到到某一批次，设置此requestId
}

/** 站内信查询结果 */
struct QueryMessageResult {
	1:list<Message> messages,
	2:PageInfo pageInfo,
}

/** 推送 */
struct PushMessage {
	1: required i32 userId,
    2: required i16 type,
    3: required string pushTitle,  			 // 推送时显示的标题
    4: optional string pushContent,			 // 推送内容
    5: optional string pushParam,  // 自定义参数
    6: optional string url,
}

/** 发送推送请求 */
struct SendPushMessageRequest {
	1: required i32 clientId,
	2: required list<PushMessage> pushMessages,
	3: optional i32 sendTime,	// 发送时间,单位秒（必须在当前时间60s以外，1年(31536000s)以内，空值或不传为立即执行）
	4: optional i64 requestId, // 如需追到到某一批次，设置此requestId
}

/** 公告广播范围 */
enum NoticeRange {
    ASSIGN,      // 指定用户
    ALL,	     // 所有
    REGUSER,     // 注册用户
    AUTHUSER,  	 // 认证用户
    NEEDUPGRADE, // 需升级用户
}

/** 公告广播类型 */
enum NoticeType {
    PUSH_AND_MESSAGE,      	    // 推送+站内信
    PUSH,		   			    // 推送
    MESSAGE,	   				// 站内信
}

/** 短信优先级 */
enum SmsPriority {
    LOWEST,
    LOW,
    NORMAL,
    HIGH,
    HIGHEST,
}

/** 短信 */
struct SmsMessage {
    1: required string mobile,				// "手机号码"
    2: required string content,			// 发送内容
    3: optional SmsPriority priority,		// 优先级 默认 Normal
    4: optional i32 sendTime,	    		// 发送时间,单位秒
}

struct BatchSmsMessage {
    1: required NoticeRange noticeRange,      // 广播范围
    2: required string content,				// 发送内容
    3: required list<string> mobiles,			// "手机号码"
    4: optional SmsPriority priority,		    // 优先级 默认 Normal
    5: optional i32 sendTime,	    			// 发送时间,单位秒
}

/** 用户类型 */
enum UserType {
    USER,		// 用户
    VISITOR,	// 访客
}

/** 站内信状态 */
enum QueryMessageStatus {
    UNREAD,		// 未读
    READ,		// 已读
    ALL,	    // 所有 排除删除
}

enum MessageStatus {
    UNREAD,		// 未读
    READ,		// 已读
    DEL,	    // 删除
}

/** 站内信查询请求 */
struct QueryMessageReq {
	1: required QueryMessageStatus queryMessageStatus, 	// 站内信状态
	2: required UserType userType,			   			    // 用户类型
	3: required string userTypeValue,						// 用户类型对应的值
	4: required list<i16> cateTypes,						// 站内信类型
	5: optional PageInfo pageInfo,
}

/** 修改站内信状态 */
struct ChgMsgStatusReq {
	1: required MessageStatus messageStatus, 				// 站内信状态
	2: required UserType userType,			   			    // 用户类型
	3: required string userTypeValue,						// 用户类型对应的值
	5: required list<i64> requestId,
}

/** 修改所有cateTypes站内信状态 */
struct ChgAllMsgStatusReq {
	1: required MessageStatus messageStatus, 				// 站内信状态
	2: required UserType userType,			   			    // 用户类型
	3: required string userTypeValue,						// 用户类型对应的值
	4: required list<i16> cateTypes,						// 站内信类型
}

/** 站内信统计信息 */
struct MessageCount {
	1:i16 cateType,
	2:string name,
	3:string icon,
	4:i32 total,
	5:string time,			//
}

/** 消息统计报告 */
struct MessageReport {
	1:i32 total,
	2:i32 success,
	3:i32 fail,
	4:i32 ing,
	5:string receiveDate,
	6:string sendDate,
	7:list<MessageSubReport> messageSubReport,
}

/** 消息统计子报告 [消息,推送,短信]*/
struct MessageSubReport {
	1:i32 total,
	2:i32 success,
	3:i32 fail,
	4:i32 ing,
	5:NoticeType noticeType,
}

struct Source {
	1:string source_id,
	2:i32 source_type,
}

/** 分页信息 */
struct PageInfo {
	1:string cursor,
	2:i16 pageCount = 20,
}

struct SysNotifyNoticeDbBean{
	1:i64 id;
    2:i64 requestId;
    3:byte notifyType;
	4:byte notifyRange;
    5:i32 sendTime;
    6:string createTime;
    7:i16 type;
    8:i16 cateType;
    9:i32 senderId;
    10:string title;
    11:string image;
    12:string content;
    13:string remark;
    14:string pushParam;
    15:byte referenceType;
    16:i32 referenceId;
    17:byte sendStatus;
}

struct NoticeListReq {
	1:byte referenceType,
	2:i32 referenceId,
	3:PageInfo pageInfo,
}

struct NoticeListResult {
	1:list<SysNotifyNoticeDbBean> notices,		// SysNotifyNoticeDbBean 数据集合
	2:NoticeListReq req,
}

/** 通知服务异常响应代码 （5000 - 6000）*/
const i16 SYSTEM_EXCEPTION = 5001

const i16 ILLEGAL_SEND_TIME_EXCEPTION = 5002

exception NotifyException {
    1: i32 errorCode,	//错误代码
    2: string message,	//错误代码描述
}

service NotifyServices {

    /**
	 * 站内信统计信息
	 * 
	 * @return list<MessageCount>
	 */     
    list<MessageCount> queryMessageCountInfo(1:QueryMessageReq queryMessageReq) throws (1:NotifyException e);
    
    /**
	 * 修改站内信消息状态
	 * 
	 */  
    void updateMessageStatus(1:ChgMsgStatusReq chgMsgStatusReq) throws (1:NotifyException e);
    
    /**
	 * 修改用户所有站内信消息状态
	 * 
	 */     
    void updateAllMessagesStatus(1:ChgAllMsgStatusReq chgAllMsgStatusReq) throws (1:NotifyException e);
    
    /**
	 * 站内信查询 
	 * 
	 * @return list<Message>
	 */   
    QueryMessageResult queryMessage(1:QueryMessageReq queryMessageReq) throws (1:NotifyException e);
    
    /**
	 * 查询[消息状态]报告
	 * 
	 * @return list<Message>
	 */      
    MessageReport queryMessageReport(1:i32 msgId) throws (1:NotifyException e);
    
	/**
	 * 系统通告 (后台使用) & (一致的 推送/站内信 内容) 群发
	 * 
	 * @return messageId
	 */     
    i64 sendSysNotice(1:SysNotice notice) throws (1:NotifyException e);
    
	/**
	 * 消息推送
	 * 
	 * @return messageId
	 */ 
    i64 sendPushMessage(1:SendPushMessageRequest sendPushMessageRequest) throws (1:NotifyException e);

	/**
	 * 发送站内信
	 * 
	 * @return messageId
	 */    
    i64 sendMessage(1:SendMessageRequest sendMessageRequest) throws (1:NotifyException e);

	/**
	 * 发送短信
	 * 
	 * @return messageId
	 */    
    i64 sendSms(1:list<SmsMessage> smsMessages) throws (1:NotifyException e);
    
	/**
	 * 批量发送短信 (消息内容一致时使用)
	 * 
	 * @return messageId
	 */
    i64 sendBatchSms(1:BatchSmsMessage batchSmsMsg) throws (1:NotifyException e);
    
	/**
	 * 绑定用户
	 * 
	 * @return messageId
	 */ 
	 void bindUser(1:string visitor,2:i32 userId) throws (1:NotifyException e);
    
	/**
	 * 测试服务 
	 * 
	 * @return “running”
	 */    
    string ping() throws (1:NotifyException e);
    
	/**
	 * 已发送列表
	 * 
	 */
	NoticeListResult getNoticeList(1:NoticeListReq req) throws (1:NotifyException e);
	
	i32 countNoticeList(1:NoticeListReq req) throws (1:NotifyException e);
}