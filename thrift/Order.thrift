namespace java com.huijiame.order.thrift
namespace php Service.Order
namespace js huijiame

//异常定义
const i16 SYSTEM_EXCEPTION = 4001 //系统异常
const i16 PARAM_EXCEPTION = 4000 //参数异常
const i16 ACTIVITY_EXCEPTION  = 4002 //活动异常
const i16 ACTIVITY_EXPIRED_EXCEPTION = 4003 //活动过期
const i16 PAY_EXCEPTION  = 4004 //支付异常
const i16 ORDER_STATUS_EXCEPTION = 4005 //订单状态异常
const i16 COUPON_EXCEPTION = 4006 //优惠券异常
const i16 INVENTORY_NOT_ENOUGH_EXCEPTION = 4007 //库存不足
const i16 NO_SUCH_ACTIVITY_OPTION_EXCEPTION  = 4008 //活动规则不存在
const i16 NO_SUCH_COUPON_EXCEPTION  = 4009 //优惠券不存在
const i16 NO_SUCH_INVENTORY_EXCEPTION  = 4010 //库存使用记录不存在
const i16 NO_SUCH_ORDER_EXCEPTION = 4011 //订单不存在
const i16 ORDER_ALREADY_EXIST_EXCEPTION  = 4012 //订单已存在
const i16 ORDER_NO_GENERATED_EXCEPTION = 4013 //无法生成订单号
const i16 PRIC_NOT_CORRECT_EXCEPTION = 4014 //价格不正确
const i16 REFUND_EXCEPTION = 4015 //退款异常
const i16 GROUP_HEADER_NOT_EXIST_EXCEPTION = 4016 //拼团-团长不存在(后台删除团长时,如果团长信息不存在,抛此异常)
const i16 GROUP_HEADER_EXIST_EXCEPTION = 4017 //拼团-团长已存在(后台添加普通用户为团长时,如果该小区已有团长,抛此异常)
const i16 GROUP_BUY_EXCEPTION = 4018 //拼团异常
//订单取消
const i16 CANCEL_EXCEPTION_4019 = 4019 //订单取消失败
const i16 CANCEL_EXCEPTION_4020 = 4020 //订单已取消,不能重复操作
const i16 CANCEL_EXCEPTION_4021 = 4021 //订单用户无权取消
//订单审核
const i16 CANCEL_CONFIRM_EXCEPTION_4022 = 4022 //订单取消审核处理失败
const i16 CANCEL_CONFIRM_EXCEPTION_4023 = 4023 //订单已被审批,不能重复操作

/**
* 活动异常
**/
exception OrderException {
    1: i32 errorCode,
    2: string message,
}

/**
* [活动/话题/Feed/达人]可见范围类型
**/
enum VisibleControlRefType{
    UNKNOWN,//未知(0)
    ACTIVITY,//活动(1)-activity
    BANNER,//广告条(2)-banner
    DOYEN,//达人(3)-doyen
    TOPIC,//话题(4)-topic
    FEED,//Feed流
}

/**
* 活动类型
**/
enum ActivityCategory{
    UNKNOWN,//未知(0)
    ACTIVITY,//活动(1),
    GROUP,//团购(2)--老版团购(未来可能会放弃)
    GROUP_BUY,//拼团(3)
}

/**
* 团购类型
**/
enum TuanType{
    NORMAL,//普通活动(0)-默认值
    STANDALONE,//独立团(1)-(ActivityCategory为拼团时,活动的TuanType必须为独立团),独立团库存的计算与小区相关
    GROUPBUY,//一起团(2),一起团库存的计算与小区无关
}


/**
* 商品类型
**/
enum CommodityType{
    SINGLE=1,//single单次(1) default:单次
    MONTH=2,//month包月(2)
}

/**
* 活动状态
**/
enum ActivityStatus{

    /**
    * 草稿箱(0)
    **/
    DRAFT,
    /**
    * 正常(1）
    **/
    NORMAL,
    /**
    * 已结束(2)-准备弃用
    **/
    FINISHED,
    /**
    * 已删除(3)-准备弃用
    **/
    DELETED
}

/**
* h_order.status 订单状态
**/
enum OrderStatus {
    TO_BE_PAIED, //待支付(0)
    BEING_PAIED, //支付中(1)
    CANCELLED,   //取消(2)
    PAIDED ,     //已支付(3)
    EXPIRED,     //过期(4)
    COMPLETED,   //已完成(5)
}

/**
* h_order.refund_status 订单表上的退款状态
**/
enum RefundStatus {
    TO_BE_REFUNDED, //待退款(0)
    BEING_REFUNDED, //退款中(1)
    FAILED,         //退款失败(2)
    COMPLETED,      //已完成(3)
}

/**
* h_order.payment_status 订单表上的支付状态
**/
enum PaymentStatus {
    TO_BE_PAIDED,   //待支付(0)
    BEING_PAIDED,   //支付中(1)
    FAILED,         //支付失败(2)
    COMPLETED,      //已完成(3)
}

/**
* h_order_refund.status 退款明细上的审核状态
**/
enum OrderRefundStatus{
    TO_BE_AUDITED,  //待审核(0)
    AUDIT_FAILED,   //审核失败(1)
    AUDIT_SUCCESS,  //审核成功(2)
}

/**
* 支付类型
**/
enum PayType {
    NONE,       //兼容(0)
    WEIXIN,     //微信APP(1)
    ALIPAY,     //支付宝APP(2)
    WEIXIN_WAP, //微信公众号支付(3)*****v2.2
    ALIPAY_WAP, //支付宝公众号支付(4)*****v2.2
}

/**
* 拼团状态*****v2.2
**/
enum GroupBuyStatus{
    UN_GROUP,//未成团(0)
    ACCOMPLISH,//已成团(1)
    CANCELED,//已取消(2)
    NOT_START,//未开团(3)
    WAIT_DELIVER,//待发货(4)
    COMPLETED,//已完成(5)
}

struct TestParam{
    1:optional bool b;
    2:optional i32 i;
    3:optional double d;
    4:optional list<i32> l;
    5:optional VisibleControlRefType v;
}

struct OrderParam {
    1: required i32 user_id, //用户id
    2: optional list<OrderStatus> status, //订单状态
    3: optional i32 size = 10, //分页数量
    4: optional i32 order_id, //起始id
    5: optional list<ActivityCategory> category,//活动类型(0-所有,1-活动,2-团购,3-拼团)
}

struct OrderAddParam {
    1: required i32 user_id, //用户id
    2: required i32 option_id, //规格id
    3: required i32 community_id, //小区id
    4: required string community_name, //小区名称
    5: optional i32 coupon_id, //优惠券id
    6: required string order_price, //价格
    7: required i32 order_count, //份数
    8: required string contact_name, //联系人名称
    9: required string contact_mobile, //联系人手机
    10: required string contact_address, //联系人地址
    11: optional string remote_ip, //客户端ip
    12: optional string remark, //备注
    13: optional bool apply_group_header,//是否愿意当团长(默认为false)*****v2.2
    14: optional string referee,//推荐人手机号码
    15: optional string utm_channel,//订单渠道来源
    16: optional i32 product_id,//活动id，字段不为空且>0时,表示开团 否则单纯下单
    17: optional string city_name,//城市名称
    18: optional string send_day,//送达时间
}


/**
*订单取消主体
*/
struct OrderCancelModel {
    1: required i32 user_id, //用户id
    2: required i32 order_id, //订单id
    3: optional string cancel_reason, //取消原因,直接传入整个文本
    4: required i32 cancel_from, //取消来源0用户,1块长取消,2不成团取消(系统)
}

/**
*订单取消审核主体
*/
struct OrderConfirmModel {
    1: required i32 user_id, 	//用户id
    2: required i32 order_id, 	//订单id
    3: required i32 refund_id, 	//审核记录id
	4: required i32 status,		//审核状态:1拒绝,2通过
	5: optional string refund_reason,//审核备注,退款理由
	6: required i32 operation_user_id,//操作员id
}

/**
* 订单主体信息*****v2.2
**/
struct Order {
    1: required i32 order_id,//运单id
    2: optional i32 user_id,//用户id
    3: optional string order_no,//订单编号
    4: optional string order_title,//订单标题
    5: optional string order_price,//订单价格
    6: optional OrderStatus order_status,//订单状态
    7: optional RefundStatus refund_status,//退款状态
    8: optional PaymentStatus payment_status,//支付状态
    9: optional string order_body,//订单主体
    10: optional string order_detail,//订单详情
    11: optional string order_time,//下单时间
    12: optional string extend_info,//扩展信息
    13: optional string remote_ip,//下单人ip
    14: optional i32 coupon_id,//优惠券
    15: optional string coupon_price,//优惠券金额
    16: optional string origin_price,//原价
    17: optional i32 product_id,//活动id
    18: optional string expired_at,//过期时间
    19: optional string contact_name,//联系人姓名
    20: optional string contact_mobile,//联系人手机
    21: optional string contact_address,//联系人地址
    22: optional string remark,//备注
    23: optional string created_at,//创建时间
    24: optional string updated_at,//更新时间
    25: optional i32 order_count,//订单数量
    26: optional i32 community_id,//小区id
    27: optional string community_name,//小区名称
    28: optional i32 option_id,//活动类型明细id
    29: optional PayType payment_from,//支付类型
    30: optional string order_type,//订单类型*****v2.2
    //31: optional string env,//部署环境(dev,test,prod)-辅助调试用*****v2.2
    31: optional string coupon_name,//优惠券名称*****v2.2
    32: optional bool is_apply_group,//是否申请过团长*****v2.2
    33: optional string referee,//推荐人手机号
    34: optional string utm_channel,//订单渠道来源
    35: optional string city_name,//城市名称
    36: optional string send_day,//送达时间
}

/**
* 订单明细
**/
struct OrderItem {
    1: required i32 item_id,
    2: required i32 order_id,
    3: optional string name,
    4: optional i32 count,
    5: optional string price,
    6: optional string total,
    7: optional byte status,//状态:1正常,2删除
    8: optional string created_at,
    9: optional string updated_at,
}

/**
* 拼团信息*****v2.2
**/
struct GroupBuyInfo{
    1:optional i32 user_id,//团长用户id
    2:optional string contact_name,//联系人
    3:optional string contact_mobile,//联系电话
    4:optional string contact_address,//联系地址
    5:optional i32 sale_num,//已售份数
    6:optional i32 left_num,//还差几份
    7:optional i32 min_num,//最低成团份数
    8:optional string end_time,//拼团结束时间,
    9:optional i64 left_seconds,//剩余秒数
    10:optional GroupBuyStatus accomplish_status,//拼团状态
    11:optional string deliver_time,//拼团发货时间
    12:optional string accomplish_time,//成团时间
    13:optional string reward,//团长奖励
    //14:optional list<i32> buy_user_ids,//抢购团成员(已支付的下单用户id列表)
}

/**
* 拼团后台管理-活动列表*****v2.2
**/
struct GroupBuyActivity{
    1:optional i64 activity_id, //团购活动ID
    2:optional string activity_title, //团购标题
    3:optional string activity_time, //发布时间
    4:optional i16 group_days,//抢购期限
    5:optional i32 order_count,//成团份数
    6:optional i32 white_list_count,//在白名单中的小区数量(0表示面向所有小区)
    7:optional i32 black_list_count,//在黑名单中的小区数量
    8:optional string coverage,//覆盖小区(全部小区|部分小区|排除小区)
    9:optional i32 community_count,//开团小区
    10:optional i32 pay_count,//支付用户
    11:optional i32 feed_count,//咨询回复
    12:optional string group_buy_status,//状态
    13:optional string admin_remark,//运营备注
    14:optional string lastest_order_time,//最后下单时间
    15:optional i32 pay_order_count,//已支付的订单份数
}

/**
* 拼团后台管理-订单列表*****v2.2
**/
struct GroupBuyAdminOrder{
    1:optional i64 order_id,//订单Id
    2:optional string order_no,//订单编号
    3:optional string contact_name,//用户姓名
    4:optional string contact_mobile,//手机号
    5:optional i64 community_id,//小区id
    6:optional string community_name,//所在小区
    7:optional string contact_address,//详细地在
    8:optional i64 option_id,//规则id
    9:optional string option_info,//规格详细（格式:规格名称^规格单价）
    10:optional string option_name,//规格名称
    11:optional double option_price,//规格单价
    12:optional i32 order_count,//份数
    13:optional double order_price,//支付金额
    14:optional PaymentStatus payment_status,//支付状态
    15:optional OrderStatus order_status,//订单状态
    16:optional string remark,//订单备注
    17:optional string order_time,//订单创建时间
    18:optional i32 user_id,//用户id
}

struct ActivityQueryParam{
    1: required i32 activity_id,//活动Id
    2: required i32 community_id,//小区Id
}

/**
* 后台拼团管理类操作（比如:添加团长,成团,发货）参数*****v2.2
**/
struct GroupAdminParam{
    1: required i32 activity_id,//活动Id
    2: optional i32 community_id,//小区Id（后台管理:成团,发货）
    3: optional i32 user_id,//运营在后台添加/删除团长时必填,其它操作可空
    4: optional byte user_type,//运营在后台添加团长时必填,0普通用户当团长,1回家么团长
    5: optional string group_end_time,//运营在后台修改某个小区拼团活动的截止时间时,必须传输,格式: yyyy-MM-dd HH:mm:ss
    6: optional GroupBuyStatus group_buy_status,//运营在后台修改某个小区拼团活动的截止时间时,必须传输
    7: optional string deliver_time,//配送时间
    8: optional string order_status,//订单状态，0待支付，1支付中，2已取消，3已支付，4已过期，5已完成查询活动订单使用,可传多个0,1,2...结尾无逗号
    9: optional string sort_field,//排序列名称
    10: optional i32 sort_desc,//排序方式,1:desc倒叙,0:asc顺序
    11: optional string accomplish_time,//成团时间
}

struct GroupBuyCommunityQueryParam{
    1: optional i32 post_user_id,//发布人id
    2: optional i32 account_id,//活动负责人(后台账号)id
    3: optional bool show_all,//是否显示全部(true是,false否,注:为true时忽略activity_status参数)
    4: optional ActivityStatus activity_status,//活动状态（0草稿箱，1为正常，2为已结束，3已删除）
    5: optional string order_clause,//排序条件（比如:update_at desc或activity_id desc,默认按activity_id desc排序）
}

/**
* 查询订单返回的对象
**/
struct OrderResult {
    1: required Order order,//订单主体信息
    2: optional OrderItem item,//订单item信息
    3: optional GroupBuyInfo group_buy,//拼团信息*****v2.2
}

/**
* 支付参数*****v2.2
**/
struct PayParam{
    1:required i32 order_id,//订单号
    2:required PayType pay_type,//支付类型
    3:optional string open_id,//支付类型为3或4时,必传
}

struct PayResult {
    1: i32 costTime,//接口消耗时长
    2: string msg,//异常信息返回
    3: string result, //返回结果
    4: string statusCode, //非200 为业务错误状态
    5: string validationErrors,
}

struct RefundResult {
    1: i32 costTime,//接口消耗时长
    2: string msg,//异常信息返回
    3: RefundSubResult result, //返回结果
    4: string statusCode, //非200 为业务错误状态
    5: string validationErrors,
}

struct RefundSubResult {
    1:i64 addTime,
    2:i32 batchNum,
    3:i32 clientId,
    4:string formFields,
    5:i32 id,
    6:string lastModifyTime,
    7:byte notifyStatus,
    8:string notifyTime,
    9:i32 platformId,
    10:string refundBatchCode,
    11:string refundTradeNo,
    12:string returnCode,
    13:string returnContent,
    14:byte status,
    15:i32 successNum,
    16:i32 userId,
}

struct Refund {
    1: required i32 refund_id,//退款申请记录id
    2: required string refund_price,//退款价格
    3: required OrderRefundStatus status,//退款审核状态(待审核0，审核失败1，审核成功2)
    4: optional string refund_reason,//退款理由
    5: required string verified_at,//申请时间
    6: required string contact_mobile,//联系电话
}

struct CallbackRefund {
    1: required i32 refund_id,
    2: required string refund_no,
    3: required RefundStatus status,
    4: optional string fail_reason,
}

struct CallbackOrderStatus {
    1: required i32 order_id,
    2: required PaymentStatus payment_status, //支付回调时更新支付状态,需要设置该状态
}

struct ApplyParam {
    1: required i32 activity_id,
    2: optional i32 community_id,
    3: optional list<OrderStatus> status,
    4: optional byte include_order_item,//返回结果中,是否包含订单item信息*****v2.2
    5: optional byte include_group_buy,//返回结果中,是否包含拼团信息*****v2.2
}

struct ActivityInventoryParam{
    1: required i32 community_id, //用户所在的小区id
    2: required list<i32> activity_ids, //活动id列表
}

struct ActivityInventoryOptionItem{
    1: i32 option_id, //option_id
    2: i32 origin_inventory,//原始库存
    3: i32 left_inventory,//剩余库存
    4: byte has_left_inventory, //是否有剩余库存(1有,0无)
}

struct ActivityInventoryItem{
    1: i32 activity_id, //活动id
    2: i32 left_inventory_sum, //总剩余库存数
    3: byte has_left_inventory,//是否有剩余库存(1有,0无)
    4: list<ActivityInventoryOptionItem> option_items,//库存明细项
}

struct GroupBuyAdminList{
    1:PageInfoResult page_info,
    2:list<GroupBuyAdminListItem> items;
}

struct PageInfoResult{
    1:i32 page_size, //每页显示条数
    2:i32 page_index, //第几页
    3:i32 page_total, //总页数
    4:i64 record_count, //总记录数
}

struct PageInfoParam{
    1:required i32 page_size, //每页显示条数
    2:required i32 page_index, //显示第几页（从1开始,page_size,page_index同时为0,表示查询全部）
}

/**
* 拼团后台管理-已成团小区列表项
**/
struct GroupBuyAdminListItem{
    1:i32 community_id,//小区id
    2:string community_name,//小区名称
    3:i32 activity_id,//活动id
    4:string activity_title,//拼团活动标题
    5:i32 order_count,//订单数量
    6:i32 group_min_num,//最低成团份数
    7:i32 pay_count,//支付人数
    8:i64 group_end_time, //活动截止时间
    9:string header_info,//团长信息
    10:i32 header_user_id,//团长id
    11:string header_name,//团长名称
    12:string header_mobile,//团长电话
    13:string header_address,//团长地址
    14:i16 accompish_status,//拼团状态
    15:string accompish_status_title,//拼团状态标题
    16:i16 is_offline,//是否下线（1下线,0正常）
    17:string is_offline_title,//是否下线(标题)
    18:string left_time,//剩余时间显示
    19:bool is_backup_header,//是否备用团长(true备用,false非备用团长)
    20:string deliver_time,//配送时间
}

struct GroupBuyAdminActivityList{
    1:PageInfoResult page_info,
    2:list<GroupBuyActivity> items;
}

struct GroupBuyAdminOrderList{
    1:PageInfoResult page_info,
    2:list<GroupBuyAdminOrder> items;
}

struct GroupBuyUnStartInfo{
    1:i32 community_count, //未开团小区数量
    2:string elasepe_time, //已发布时长
}

/**
* 后台管理-活动列表查询参数(为后台管理列表预留,暂未实现)
**/
struct ActivityAdminQueryParam{
    1:TuanType tuan_type,//结伴模式（1/独立团,2/一起团,0显示全部）
    2:ActivityCategory category,//类型（1/活动,2团购,3拼团,0显示全部）
    3:i32 apply_status,//报名状态（1报名中,2报名截止,3精彩回顾,0显示全部）
    4:i32 status,//活动状态（0草稿箱,1发布的活动,2下线的活动,3取消的活动）
    5:i32 account_id//联系人账号（0不限）
}

struct KeyValue{
    1:i32 key,
    2:string value,
}

/**
* 添加/修改 活动的参数对象*****v2.2
**/
struct Activity{
    1:optional i32 activity_id,//活动id
    2:optional i32 post_user_id,//主办者账号(h_user的)id
    3:optional i32 ref_activity_id,//关联活动id
    4:required ActivityCategory category,//活动类型
    5:required string activity_title,//活动标题
    6:optional string activity_time,//活动举办时间(category为活动时,必填)
    7:optional string activity_end_time,//活动举办结束时间(category为[活动|团购]时,必填)
    8:optional string activity_apply_end_time,//报名/抢购截止时间(category为[活动|团购]时,必填)
    9:required string city_name,//城市名
    10:optional string address,//活动举办详细地址
    11:optional string sale_point,//卖点
    12:optional string reward,//奖励
    13:optional list<KeyValue> white_list,//小区白名单(在该列表中的小区可见),key为小区id,value为小区名称
    14:optional list<KeyValue> black_list,//小区黑名单(在该列表中的小区不可见)-黑白名单同时为空时,表示本城市所有小区均可见,key为小区id,value为小区名称
    15:required TuanType tuan_type,//团购的库存类型
    16:optional i32 people_min_num,//最低成团人数
    17:optional i32 group_buy_min_num,//最低成团份数(拼团活动专用)
    18:optional i16 group_buy_days,//拼团有效期(单位:天)
    19:optional bool restrict_inventory,//是否限制库存(true限制库存,false不限制,即允许超卖, 默认false)
    20:required list<ActivityOption> activity_options,//活动规格
    21:required list<ActivityPics> activity_pics,//活动图片列表
    22:required list<ActivityContent> activity_contents,//活动内容
    23:optional i32 group_buy_header_user_id,//备用团长的userId(拼团活动时,可指定一个备用团长的马甲账号id)
    24:required ActivityStatus status,//状态
    25:required i32 account_id,//活动负责人(后台账号)id
    26:optional string unit,//销售单位（盒,箱之类）
    27:optional bool is_pinned,//是否置顶(true置顶,false不置顶,默认为false)
    28:optional string group_buy_header_mobile, //备用团长电话
    29:optional string group_buy_header_name, //备用团长名字
    30:optional i32 ref_topic_id,//关联话题id
    31:optional i16 apply_verify,//仅认证用户可见(1是,0否)
    32:optional string admin_remark,//后台自己看的备注
    33:optional i32 order_count_max,//用户在前台每次下单时的最大购买数量
    34:optional i32 product_id,//模板活动的活动id
    35:optional string commodity_remark,//商品备注
    36:optional string utm_channel,//拼团渠道来源
    37:optional i32 sku,//商品品类,default:0其他,1鲜花
    38:optional CommodityType commodity_type,//商品类型(频率),default:1单次,2包月
    39:optional list<ActivityPics> activity_small_pics,//活动小图
}

/**
* 活动内容*****v2.2
**/
struct ActivityContent{
    1:optional i32 content_id,//内容id
    2:optional i32 activity_id,//活动id
    3:required i16 order_num,//排序号
    4:required string category,//类别-[活动摘要|费用说明|不成团说明|注意事项]之一
    5:optional string font_style,//字体样式(貌似2.x被放弃,没有传空)
    6:optional string font_size,//字体大小-[large|medium|small]之一
    7:optional string font_color,//字体颜色-类似:#ffffff
    8:optional byte font_align,//对齐方式-left:1,center:2,right:3
    9:optional byte font_bold,//是否加粗-(1加粗,0正常)
    10:optional byte font_italic,//是否斜体-(1斜体,0正常)
    11:required string content,//内容文本
    12:optional list<ActivityPics> pics,//内容图片
}

/**
* 活动规格*****v2.2
**/
struct ActivityOption{
    1:optional i32 option_id,//规格id
    2:optional i32 activity_id,//活动id
    3:required string option_name,//规格名称
    4:required double option_price,//规格单价
    5:required double origin_price,//规格原价
    6:required i32 inventory,//原始库存
    7:optional bool is_delete,//是否已删除(修改活动时,如果删除了该option,本项设置为true)
}

/**
* 活动图片*****v2.2
**/
struct ActivityPics{
    1:optional i32 pic_id,//图片id
    2:optional i32 content_id,//内容id
    3:optional i32 activity_id,//活动id
    4:required string pic_url,//图片地址
    5:required i32 pic_width,//图片宽度
    6:required i32 pic_height,//图片高度
}

/**
* 订单/库存/活动服务
**/
service OrderServices {

    /**
    * 测试thrift服务(专用)
    **/
    string ping() throws (1:OrderException e),

    /**
    * 测试参数类型(内部专用)
    **/
    string testParameter(1:bool b,2:i32 i,3:double d,4:list<i32> l,5:VisibleControlRefType v) throws (1:OrderException e),

    /**
    * 测试参数类型(内部专用)
    **/
    string testParameter2(1:TestParam v) throws (1:OrderException e),

    /**
    * 添加对象可见性(注:白名单优先于黑名单,如果黑/白名单均为空,表示所有小区可见)
    * @refType 类型(活动/Banner/话题之类,详见VisibleControlRefType枚举定义)
    * @refId 与上面的类型对应,如果refType为活动,则这里为活动id
    * @whiteList 白名单小区列表(此列表中的小区,可见)
    * @blackList 黑名单小区列表(此列表中的小区,不可见)
    **/
    void addVisibleControl(1:VisibleControlRefType refType,2:i32 refId, 3:list<KeyValue> whiteList,4:list<KeyValue> blackList) throws (1:OrderException e),

    /**
    * 新增逻辑小区
    * @logicCommunityName: 逻辑小区名字
    * @communityIds: 待添加的小区id列表
    * @defaultCommunityId: 默认的小区id（注:新增后的逻辑小区,地址/经纬度等信息与默认小区完全相同),该id必须在communityIds范围内
    * @return 返回新增记录的逻辑小区id
    **/
    i32 addLogicCommunity(1:string logicCommunityName,2:list<i32> communityIds,3:i32 defaultCommunityId) throws (1:OrderException e),

    /**
    * 更新逻辑小区
    * @logicCommunityId: 逻辑小区Id
    * @communityIds: 物理小区id列表
    * @defaultCommunityId: 默认物理小区Id
    **/
    void updateLogicCommunity(1:i32 logicCommunityId,2:string logicCommunityName,3:list<i32> communityIds,4:i32 defaultCommunityId) throws (1:OrderException e),

    /**
    * 删除逻辑小区
    * @logicCommunityId: 逻辑小区Id
    **/
    void deleteLogicCommunity(1:i32 logicCommunityId) throws (1:OrderException e),

    /**
    * 后台-查询单个活动*****v2.2
    **/
    Activity getActivityById(1:i32 activityId) throws (1:OrderException e),

    /**
    * 后台-创建活动*****v2.2
    **/
    Activity addActivity(1:Activity param) throws (1:OrderException e),

    /**
    * 后台-编辑活动*****v2.2
    **/
    Activity updateActivity(1:Activity param) throws (1:OrderException e),

    /**
    * 查询拼团信息*****v2.2
    **/
    GroupBuyInfo getGroupBuyInfo(1:ActivityQueryParam param) throws (1:OrderException e),

    /**
    * 拼团后台-查询团购活动列表*****v2.2
    **/
    GroupBuyAdminActivityList getGroupBuyActivities(1:GroupBuyCommunityQueryParam queryParam,2:PageInfoParam pageParam) throws (1:OrderException e),

    /**
    * 拼团后台-查询某团购活动下的订单列表*****v2.2
    **/
    GroupBuyAdminOrderList getGroupBuyAdminOrders(1:GroupAdminParam queryParam,2:PageInfoParam pageParam) throws (1:OrderException e),

    /**
    * 拼团后台-添加团长*****v2.2
    **/
    void addGroupHeader(1:GroupAdminParam param) throws (1:OrderException e),

    /**
    * 拼团后台-删除团长*****v2.2
    **/
    void deleteGroupHeader(1:GroupAdminParam param) throws (1:OrderException e),

    /**
    * 拼团后台-强制成团*****v2.2
    **/
    void forceGroup(1:GroupAdminParam param) throws (1:OrderException e),

    /**
    * 拼团后台-单个小区下线*****v2.2
    **/
    void offlineGroupBuy(1:i32 activity_id,2:i32 community_id) throws (1:OrderException e),

    /**
    * 拼团后台-批量取消未开团的小区*****v2.2
    **/
    void batchCancelUnStartGroupBuy(1:i32 activityId) throws (1:OrderException e),

    /**tep
    * 拼团后台-未成团的单个小区,人工取消*****v2.2
    **/
    void cancelGroup(1:GroupAdminParam param) throws (1:OrderException e),

    /**
    *更改活动成团成团进度信息<br/> 
    *可更改:成团状态,成团时间,结束时间,活动的发货时间<br/>
    *方法更改规则如下:<br/>
    * 成团状态=1时,如果活动成团时间不存在且也没传时,成团时间为当前时间<br/>
    * 成团状态=5时,如果活动结束时间不存在且没传 或者 结束时间>当前时间,结束时间更改为当前时间<br/>
    **/
    void updateActivityAccomplishInfo(1:GroupAdminParam param) throws (1:OrderException e),

    /**
    * 后台管理-取消并下线某活动*****v2.2
    **/
    void cancelAndOfflineGroup(1:GroupAdminParam param) throws (1:OrderException e),

    /**
    * 拼团后台-获取已开团小区列表*****v2.2
    **/
    GroupBuyAdminList queryGroupBuyCommunityList(1:i32 activityId,2:PageInfoParam pageParam) throws (1:OrderException e),

    /**
    * 拼团后台-获取已成团小区列表*****v2.2
    **/
    GroupBuyAdminList queryGroupBuySuccessCommunityList(1:i32 activityId,2:PageInfoParam pageParam) throws (1:OrderException e),

    /**
    * 拼团后台-获取未开团小区数量*****v2.2
    **/
    GroupBuyUnStartInfo queryUnStartGroupBuyCommunityCount(1:i32 activityId) throws (1:OrderException e),

    /**
    * 获取某个活动下的报名用户列表
    **/
    list<OrderResult> getApplyOrders(1:ApplyParam param) throws (1:OrderException e),

    /**
    * 获取某小区的所有活动库存明细*****v2.2
    **/
    list<ActivityInventoryItem> getActivityInventory(1:ActivityInventoryParam param) throws (1:OrderException e),

    /**
    * 获取用户订单数
    **/
    i32 getOrderNum(1:OrderParam param) throws (1:OrderException e),

    /**
    * 获取用户的订单列表
    **/
    list<OrderResult> getOrders(1:OrderParam param) throws (1:OrderException e),

    /**
    * 获取订单详情
    **/
    OrderResult getOrder(1:i32 orderId) throws (1:OrderException e),

    /**
    * 添加订单,返回order id
    **/
    OrderResult addOrder(1:OrderAddParam order) throws (1:OrderException e),
	
	/**
	*add date 2016-04-05
    *提供免费下单功能:暂时后台业务[我要开团]功能使用,返回order_id
    **/
    OrderResult addFreeOrder(1:OrderAddParam order) throws (1:OrderException e),
    
    /**
    * 取消订单<br>
    **/
    void cancelOrder(1:OrderCancelModel orderCancelModel)throws (1:OrderException e),

	/**
    * 取消订单审核<br>
    **/
	void cancelOrderConfirm(1:OrderConfirmModel orderConfirmModel) throws (1:OrderException e),
    
    /**
    * 微信app或支付宝app支付(不支持公众号支付)
    **/
    PayResult pay(1:i32 orderId,2:PayType payType) throws (1:OrderException e),

    /**
    * 微信或支付宝公众号支付*****v2.2
    **/
    PayResult payOrder(1:PayParam param) throws (1:OrderException e),

    /**
    * 批量申请退款
    **/
    void addRefunds(1:list<i32> orderIds) throws (1:OrderException e),

    /**
    * 批量审核退款记录
    **/
    void auditRefunds(1:list<Refund> refunds) throws (1:OrderException e),

    /**
    *退款/重新退款接口
    *anewRefund:不传或0时为退款,1:重新退款
    **/
    RefundResult refund(1:i32 userId, 2:list<i32> refundIds,3:i32 anewRefund) throws (1:OrderException e),

    /**
    * 异步更改退款状态和refund_no
    **/
    void callbackUpdateRefund(1:CallbackRefund refund) throws (1:OrderException e),

    /**
    * 异步更改支付状态
    **/
    void callbackUpdateOrderStatus(1:CallbackOrderStatus status) throws (1:OrderException e),

}
