admin_dev:
	rsync -avzP --exclude-from=.excludes ./* deploy@devadmin.huijiame.com:/var/www/html/api/huijiame/admin/thrift_contract
admin_test:
	rsync -avzP --exclude-from=.excludes ./* deploy@testadmin.huijiame.com:/var/www/html/api/huijiame/admin/thrift_contract
admin_prod:
	rsync -avzP --exclude-from=.excludes ./* deploy@deploy.tool.957e040e50004092yg.custom.ucloud.cn:/var/www/html/api/huijiame/admin/thrift_contract

api_dev:
	rsync -avzP --exclude-from=.excludes ./* deploy@devapi.huijiame.com:/var/www/html/api/huijiame/master2/thrift_contract
api_test:
	rsync -avzP --exclude-from=.excludes ./* deploy@testapi.huijiame.com:/var/www/html/api/huijiame/master2/thrift_contract
api_prod:
	rsync -avzP --exclude-from=.excludes ./* deploy@api1.service.957e040e50004092yg.custom.ucloud.cn:/var/www/html/api/huijiame/master2/thrift_contract
	rsync -avzP --exclude-from=.excludes ./* deploy@api2.service.957e040e50004092yg.custom.ucloud.cn:/var/www/html/api/huijiame/master2/thrift_contract
